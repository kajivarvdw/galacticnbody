        Galactic N-Body simulations with CUDA


These are the release notes for the galacticNBody software version
1.0. Read them carefully, as they tell you what this is all about,
explain how to install the software and what the options are.


WHAT DOES THIS SOFTWARE DO?

    This software allows to simulate the creation and evolution of
    galaxies using general purpose computers, especially CUDA enabled
    devices.

    It is distributed under the GNU General Public License.

ON WHAT HARDWARE DOES IT RUN?

    This software is optimized for normal computers and laptops
    which have a CUDA enabled GPU. These are produced by NVIDIA.
    The faster your hardware, the faster these simulations will
    run. The software is only tested on Linux, but should also
    work on other operating systems.

INSTALLING THE SOFTWARE:

    - If you want to make use of the CUDA acceleration, you need
      the CUDA Toolkit, available at:
      https://developer.nvidia.com/cuda-downloads.
      This will install the nvcc compiler, which we will use to
      compile the software. To compile the software, first go 
      to the GPU directory:

            cd ~/path/to/source/gpu

      Then review the options in the file galaxySimulation.cu.
      When you're satisfied with the options, compile the sofware
      with:
      
            nvcc galaxySimulation.cu -lm -o galaxySimulation

      If there are no errors, you are ready to run your simulation.

    - If you only want to use the CPU version, you will need the 
      gcc compiler (or another C compliant compiler). Go the CPU
      directory:
        
            cd ~/path/to/source/cpu

      Again, review the options in the file galaxySimulation.c.
      Note that this is a .c file, and not a CUDA .cu file. Once
      you're ready, compile the file with:

            gcc galaxySimulation.c -lm -o galaxySimulationCPU

      You're ready to go!

AVAILABLE OPTIONS   

    - When you open one of the source files, you will find several
      options available right away. You can change the number of steps
      by changing the line "#define TIMESTEPS XX".
    - By default, the software will create a galaxy with 5120 stars
      randomly distributed with a Gaussian distribution function.
      You can change this on line 193 to use a uniform radial distribution.
    - It is also possible to simulate the interactions between several
      galaxies.
    - Most of the other options are obvious from their names.

    - The software also automatically creates a file 'rotationCurve.dat'.
      This contains the distance to the centre of the gravity and the
      speed of that star. This is useful to visualize the Galaxy
      Rotation Curve.
    
    Another useful option is the Start Where You Left Off option. This
    allows you to continue a simulation where you left off, for example
    because you ran out of time. At the end of every simulation, there
    is a new file 'output/finalConditions.bin' which contains the needed
    information. To start with these conditions, start the software as follows:

        ./galaxySimulation output/finalConditions.bin

    It will automatically load the conditions from the given file, and
    start off with that.
