/********************************************
 * Creates the initial stars for a galaxy simulation
 * This model only uses 2 dimensions
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define N 5120 
#define THREADS_PER_BLOCK 512 

#define PI 3.14159265359
#define GALAXYSIZE 27000.   /* In lightyears */
#define SUNMASS 2.          /* In solar masses (1.9e30 kg) */
#define G 2*1.559e-13f      /* In (ly)^3/(solar mass * year^2) (Should be: 1.559e-13) (7.84e-44 in ly,yr,kg) */
#define Gspeed 1.559e-13f   /* The correct value for G should still be used for the initial speeds */

#define STEPSIZE 1.e9f      /* Each step is 1 billion years */
#define TIMESTEPS 10 

typedef struct Body {
        float mass;
        float x[2];    /* Position vector */
        float v[2];    /* Velocity vector */
        float f[2];    /* Force vector */
} Body;


/* Declare used functions (defined at the end) */
void randPosition(Body *b, float *startingPos);
void randPositionUnif(Body *b, float *startingPos);
void randMass(Body *b);
void initVelocity(Body *b, FILE *file, float *startingPos, float *startingVelocity);


/**************
 * Functions needed for VelocityVerlet integration
 **************/

/* Function to set the total force to 0 */
__host__ __device__ void resetForce (Body *b) {
    int i;
    for (i = 0; i < 2; i++) b->f[i] = 0.f;
}
 
 
/* Update the position a body */
__device__ void updatePos (Body *b, float dt) {
    int i;
    for (i = 0; i < 2; i++) {
        b->x[i] += dt * b->v[i];
    }
}
 
 
/* Calculate the force from b2 to b1 */
__host__ __device__ void addForce (Body *b1, Body *b2) {
    float r[2];
    float dist, force;
    int i;
    float eps = 1.e3;
     
    /* Calculate the distance vector */
    for (i = 0; i < 2; i++) r[i] = b2->x[i] - b1->x[i];
     
    dist = sqrtf(r[0]*r[0] + r[1]*r[1] + eps*eps);
    force = G * b1->mass * b2->mass / (dist * dist * dist);
     
    /* Update the force */
    for (i = 0; i < 2; i++) b1->f[i] += force * r[i];
     
}
 
/* Finally upe the velocity of a body */
__device__ void updateVelocity (Body *b, float dt) {
    int i;
    for (i = 0; i < 2; i++) b->v[i] = b->v[i] + dt * b->f[i] / (2.f*b->mass);
}





/**************
 * CUDA Kernel
 **************/
__global__ void calcStep (Body *bodies) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int j,k;
    float dt = STEPSIZE;
    Body *b = &bodies[index];

    /* Calculate 5 steps before copying back the results to the host */
    for (k = 0; k < 5; k++) {

        updateVelocity(b,dt);
        resetForce(b);
        updatePos(b, dt);
        __syncthreads();
        for (j = 0; j < N; j++) {
            if (index != j) addForce(b, &bodies[j]);
        }
        __syncthreads();

        updateVelocity(b, dt);
        
    }
}



int main (int argc, char *argv[]) {
    



    /**************
     * Initialize all the variables
     **************/

    /* Calculate necessary amount of memory */
    int gpuBlockSize = 1,
        bodySize = sizeof(Body),
        numBytes = N * bodySize,
        gpuGridSize = N / gpuBlockSize;
    printf("bodySize: %i, gpuGridSize: %i\n", bodySize, gpuGridSize);

    /* Create pointers for every body */
    Body *cpuBodies, *gpuBodies;

    /* Allocate memory for N bodies on both the CPU and GPU */
    cpuBodies = (Body*)malloc(numBytes);
    cudaMalloc((void**)&gpuBodies, numBytes);

    /* Declare all other variables */
    int i,t;
    float startingPos[2], startingVelocity[2];

    /* Initialize the output file */
    FILE *output, *rotationCurve, *final;

   
   
   
    
    /**************
     * Initialize the N bodies with starting positions and velocities
     **************/
    
    /* First check if an input filename has been provided for initial conditions */
    if (argc == 2) {
        printf("Importing initial conditions from %s...\n", argv[1]);
        FILE *input;
        size_t readResult;
        input = fopen(argv[1], "rb");
        if (input == NULL) {
            printf("Error!\n");
            exit(0);
        }
        readResult = fread(cpuBodies,sizeof(struct Body),N,input);
        if (readResult != 0) printf("Success. Starting simulation.\n");
        output = fopen("output/output.dat","a");
    } else { /* Otherwise create random particles */
        
        rotationCurve = fopen("output/rotationCurve.dat","w");
        output = fopen("output/output.dat","w");
    
        /* Seed for the random generation */
        srand(time(NULL));

        /* Set position and velocity of centres of galaxies */
        startingPos[0] = 0.;    startingVelocity[0] = 0.;
        startingPos[1] = 0.;    startingVelocity[1] = 0.;

        /* Generate normally distributed bodies */
        
        /**************
         * Choose here if you want more than one galaxy
         * For two galaxies, you would have i < N/2.
         **************/
        for (i = 0; i < N; i++) { 
        //for (i = 0; i < N/2; i++) { 

            resetForce(&cpuBodies[i]);
            
            /**************
             * Choose here between Gaussian normal distributed
             * or uniformly distributed initial positions
             **************/
            randPosition(&cpuBodies[i], &startingPos[0]);       /* Gaussian */
            //randPositionUnif(&cpuBodies[i], &startingPos[0]); /* Uniformly */

            randMass(&cpuBodies[i]);
            initVelocity(&cpuBodies[i], rotationCurve, &startingPos[0], &startingVelocity[0]);
        }


        /**************
         * Use the following lines for the initial conditions of a second galaxy 
         **************/

        /* Set position and velocity of centres of galaxies */
        //startingPos[0] = 3.e5;    startingVelocity[0] = -2.5e-9;
        //startingPos[1] = -2.e5;    startingVelocity[1] = 0.;

        /* Generate normally distributed bodies */
        //for (i = N/2; i < N; i++) { 
        //    resetForce(&cpuBodies[i]);
        //    randPosition(&cpuBodies[i], &startingPos[0]);
        //    randMass(&cpuBodies[i]);
        //    initVelocity(&cpuBodies[i], rotationCurve, &startingPos[0], &startingVelocity[0]);
        //}


        fclose(rotationCurve);
        
    }


    /* Copy the bodies to the GPU */
    cudaMemcpy(gpuBodies, cpuBodies, numBytes, cudaMemcpyHostToDevice);


    
    /**************
     * Execute the simulation itself
     **************/

    /* Copy the result back to host memory */
    
    for (t = 0; t < TIMESTEPS; t++) {
        calcStep<<< N/THREADS_PER_BLOCK, THREADS_PER_BLOCK >>>( gpuBodies );
        cudaMemcpy(cpuBodies, gpuBodies, numBytes, cudaMemcpyDeviceToHost);
        printf("Saving timestep %i\n",t);
        for (i = 0; i < N; i++) fprintf(output,"%e %e %e %e\n", cpuBodies[i].x[0], cpuBodies[i].x[1], cpuBodies[i].f[0], cpuBodies[i].f[1]);
    }
    
    cudaMemcpy(cpuBodies, gpuBodies, numBytes, cudaMemcpyDeviceToHost);

    /* Write the final conditions to a file to enable continued simulation */

    final = fopen("output/finalConditions.bin","w");
    fwrite(cpuBodies, bodySize, N, final);
    




    
    /**************
     * End of program, deallocate memory
     **************/

    free(cpuBodies);
    cudaFree(gpuBodies);

    fclose(output);
    fclose(final);

    
}



void randPosition (Body *b, float *startingPos) {
    float u[2];
    /* Start by generating two uniformly distributed random floats */
    u[0] = (float)rand()/(float)RAND_MAX;
    u[1] = (float)rand()/(float)RAND_MAX;
    /* Than Box-Muller-transform them to normally distributed variables */
    b->x[0] = sqrt(-2.*log(u[0]))*cos(2*PI*u[1]) * GALAXYSIZE + startingPos[0]; /* Position in ly */
    b->x[1] = sqrt(-2.*log(u[0]))*sin(2*PI*u[1]) * GALAXYSIZE + startingPos[1];
}


/* Generates uniformly distributed random positions */
void randPositionUnif (Body *b, float *startingPos) {
    float u[2];
    /* Start by generating two uniformly distributed random floats between 0. and 1. */
    u[0] = (float)rand()/(float)RAND_MAX;
    u[1] = (float)rand()/(float)RAND_MAX;
    /* Than Box-Muller-transform them to normally distributed variables */
    b->x[0] = (u[0]*1.e5)*cos(u[1]*2*PI) + startingPos[0];
    b->x[1] = (u[0]*1.e5)*sin(u[1]*2*PI) + startingPos[1];
}


void randMass (Body *b) {
    float u[2];
    /* Similar to randPosition */
    u[0] = (float)rand()/(float)RAND_MAX;
    u[1] = (float)rand()/(float)RAND_MAX;
    /* Than Box-Muller-transform them to normally distributed variables */
    b->mass = (sqrt(-2.*log(u[0]))*cos(2*PI*u[1]) * 0.2 * SUNMASS + SUNMASS); /* Mass in kg */
}


void initVelocity (Body *b, FILE *file, float *startingPos, float *startingVelocity) {
    float dist, r[2];
    float speed, totalMass, sigma, mass, rc, r0;
    int i;
    /* First calculate the distance between the body and the centre of the galaxy */
    for (i = 0; i < 2; i++) r[i] = b->x[i] - startingPos[i];
    dist = sqrt(r[0]*r[0]+r[1]*r[1]);

    /* The total mass within a radius r of the centre is needed for the speed calculation */
    totalMass = N*SUNMASS;
    sigma = GALAXYSIZE;
    mass = totalMass*erff(dist/(sqrt(2.)*sigma));

    /* Rotational velocity (Brownstein, Moffat 2005) */
    rc = 5.e3; r0 = 27000.;
    speed = sqrt(Gspeed*totalMass/dist)*sqrt(dist/(rc+dist))*(dist/(rc+dist)*(dist/(rc+dist)))*sqrt(1+sqrt(mass/totalMass)*(1-exp(-dist/r0))*(1+dist/r0));

    /* Save speed for the Galaxy Rotation Curve */
    fprintf(file,"%e %e\n",dist,speed);

    b->v[0] = -speed * r[1] / dist + startingVelocity[0];  /* Velocity in ly/year */
    b->v[1] = speed * r[0] / dist + startingVelocity[1];
}




